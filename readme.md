# Таймлайн работ и происшествий

## Постановка задачи

Календарь для составления расписания работ, ведущихся на разных сервисах заведения сбоев.  
Можно подписаться на уведомления по кокретному сервисов или по тэгу (“хочу получать уведомления работах и происшествиях 
на сервисах, с тэгом “оплата“).  
Если кто-то завёл сбой на какой-то момент, все, кто проводил/проводит работы получают уведомление.

## Описание сервиса
Сервис предоставляет REST API конечным пользователям для создания/изменения/просмотра событий по различным сервисам,
подписки для получения уведомлений по событиям/сервисам/тегам событий.  
Основной сервис отвечает за менеджент событий и отправки команд на уведомление в отдельный сервис для рассылки.  
Сервис для реализации рассылки уведомлений - абстракция с внешним API, для получения команды и данных на отправку в
момент. Предполагается, что это абстракция будет реализована простым сервисом-моком для демонстрации работы основного,
которая иногда будет изображать неудачу при отправке сообщений.  

## Описание сущностей/таблиц

Сущность описания события **Event**:  
* id              - уникальный идентификатор события  
* typeId          - тип события, внешний ключ из таблицы типов событий EventType  
* serviceId       - сервис, накотором проходит событие, внешний ключ из таблицы сервисов Service  
* statusId        - статус, в котором находится событие, внешний ключ из таблицы статусов EventStatus  
* startDateTime   - дата/время начала события  
* endDateTime     - дата/время окончания события  
  
TODO иерархия отключения/зависимости сервисов  
TODO повторяющиеся события  
  
Сущность описания сервиса **Service**:  
* id      - уникальный идентификатор сервиса  
* name    - название сервиса  
  
Сущность описания тега **ServiceTag**:
* id      - уникальный идентификатор тега  
* name    - название тега  
  
Связь между **Service** и **ServiceTag** многие ко многим  
  
Сущность описания пользователя **User**:  
* id          - уникальный идентификатор пользователя  
* mail        - адрес почты пользователя      
* passHash    - хеш-значение для пароля пользователя  
  
Сущность описания уведомления **Notification**:  
* id      - уникальный идентификатор типа уведомления  
  
Связь между **Notification** и **User** многие ко многим  
  
Сущность описания уведомления **NotificationForUser**:  
* notificationId      - уникальный идентификатор уведомления 
* userId              - уникальный идентификатор пользователя
* enabled             - флаг того, что отправка уведомления включена
  
  
## REST API основного сервиса  
  
PUT /event                  - Создание события  
POST /event/{eventId}       - Изменение события  
DELETE /event/{eventId}     - Удаление события  
GET /event/{eventId}        - Получение деталей события  
  
GET /event/list/tag/{tagId}     - Получение списка событий по тегу  
  
PUT /notification/subscribe/tag/{tagId}             - Подписаться на уведомления по тегу  
PUT /notification/subscribe/service/{serviceId}     - Подписаться на уведомления по сервису  
PUT /notification/subscribe/event/{eventId}         - Подписаться на уведомления по событию  
  
POST /notification/{notificationId}         - Изменение правил подписки на уведомления  
DELETE /notification/{notificationId}       - Удаление подписки на уведомления  
GET /notification/{notificationId}          - Получение деталей подписки на уведомления  
  
GET /notification/list     - Получение списка событий для авторизованного пользователя  

