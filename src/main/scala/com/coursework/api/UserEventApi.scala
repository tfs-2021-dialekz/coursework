package com.coursework.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.LongNumber
import akka.http.scaladsl.server.Route
import com.coursework.model.Event
import com.coursework.service.EventService

class UserEventApi(eventService: EventService) extends ApiJsonSupport {

  val getEvent: Route =
    (get & path("event" / LongNumber)) {
      eventId => complete(eventService.getEventById(eventId))
    }

  val createEvent: Route =
    (put & path("event" / "create") & entity(as[Event])) {
      event => complete(eventService.createEvent(event))
    }

  val updateEvent: Route =
    (post & path("event" / LongNumber / "update") & entity(as[Event])) {
      (eventId, event) => complete(eventService.updateEvent(eventId, event))
    }

  val startEvent: Route =
    (post & path("event" / LongNumber / "start")) {
      eventId => complete(eventService.startEvent(eventId))
    }

  val finishEvent: Route =
    (post & path("event" / LongNumber / "finish")) {
      eventId => complete(eventService.finishEvent(eventId))
    }

  val rejectEvent: Route =
    (post & path("event" / LongNumber / "reject")) {
      eventId => complete(eventService.rejectEvent(eventId))
    }

  val route: Route = {
    getEvent ~
      createEvent ~
      updateEvent ~
      startEvent ~
      finishEvent ~
      rejectEvent
  }
}
