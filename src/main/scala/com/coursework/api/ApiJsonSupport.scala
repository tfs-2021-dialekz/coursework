package com.coursework.api

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.coursework.model.EventStatus.EventStatus
import com.coursework.model.EventType.EventType
import com.coursework.model._
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import spray.json.{DefaultJsonProtocol, JsNumber, JsString, JsValue, JsonFormat}

trait ApiJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val eventTypeFormat = new JsonFormat[EventType] {
    override def write(obj: EventType.Val): JsValue = JsString(obj.name)
    override def read(json: JsValue): EventType.Val = json match {
      case JsString(str) => EventType.byName(str)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse String")
    }
  }

  implicit val eventStatusFormat = new JsonFormat[EventStatus] {
    override def write(obj: EventStatus.Val): JsValue = JsNumber(obj.code)
    override def read(json: JsValue): EventStatus.Val = json match {
      case JsNumber(num) => EventStatus.byCode(num.intValue)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse Number")
    }
  }

  implicit val dateTimeFormat = new JsonFormat[DateTime] {
    override def write(obj: DateTime): JsValue = JsString(obj.toString(ISODateTimeFormat.dateTime))
    override def read(json: JsValue): DateTime = json match {
      case JsString(str) => DateTime.parse(str, ISODateTimeFormat.dateTime)
      case x => throw new RuntimeException(s"Unexpected type ${x.getClass.getName} when trying to parse DateTime")
    }
  }

  implicit val eventFormat = jsonFormat5(Event)
}
