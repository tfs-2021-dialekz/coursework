package com.coursework.model

import com.coursework.model.EventStatus.EventStatus
import com.coursework.model.EventType.EventType
import com.github.nscala_time.time.Imports.DateTime

final case class Event(id: Option[Long],
                       eventType: EventType,
                       status: EventStatus,
                       plannedStartTime: DateTime,
                       plannedFinishTime: DateTime)
