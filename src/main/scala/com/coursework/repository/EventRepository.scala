package com.coursework.repository

import com.coursework.model.EventType.EventType
import com.coursework.model.EventStatus.EventStatus
import com.coursework.model._
import org.joda.time.DateTime
import slick.jdbc.H2Profile.api._
import slick.lifted.{ProvenShape, Rep, TableQuery, Tag}

class EventsTable(tag: Tag) extends Table[Event](tag, "event") {

  def id: Rep[Long] = column("id", O.PrimaryKey, O.AutoInc)
  def eventType: Rep[EventType] = column("type_id")
  def status: Rep[EventStatus] = column("status_id")
  def plannedStartTime: Rep[DateTime] = column("planned_start_time")
  def plannedFinishTime: Rep[DateTime] = column("planned_finish_time")

  override def * : ProvenShape[Event] = (
    id.?,
    eventType,
    status,
    plannedStartTime,
    plannedFinishTime
    ).mapTo[Event]
}

class EventRepository {

  val allEvents = TableQuery[EventsTable]

  def createTableForLocal = db.run(allEvents.schema.create)

  def add(event: Event): DIO[Event, Effect.Write] =
    (allEvents returning allEvents.map(_.id)
      into ((event, eventId) => event.copy(id = Some(eventId)))
      ) += event

  def getById(eventId: Long): DIO[Option[Event], Effect.Read] =
    allEvents
      .filter(_.id === eventId)
      .result
      .headOption

  def updateById(eventId: Long, event: Event): DIO[Int, Effect.Write] =
    allEvents
      .filter(_.id === eventId)
      .update(event)

  def updateStatusById(eventId: Long, eventStatus: EventStatus.Val): DIO[Int, Effect.Write] =
    allEvents
      .filter(_.id === eventId)
      .map(_.status)
      .update(eventStatus)
}
