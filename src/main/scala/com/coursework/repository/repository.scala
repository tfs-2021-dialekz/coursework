package com.coursework

import com.coursework.model.EventStatus.EventStatus
import com.coursework.model.EventType.EventType
import com.coursework.model._
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import slick.dbio.{DBIO, DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.MappedColumnType
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import java.util.UUID

package object repository {
  type DIO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
  val DIO = DBIO

  val dbH2Local = Database.forURL(
    s"jdbc:h2:mem:${UUID.randomUUID}",
    driver = "org.h2.Driver",
    keepAliveConnection = true
  )
  val courseworkDB = Database.forConfig("courseworkDB")

  val db = courseworkDB

  implicit val eventTypeMapper = MappedColumnType.base[EventType, Int](
    eventType => eventType.id,
    typeId => EventType.byId(typeId)
  )

  implicit val eventStatusMapper = MappedColumnType.base[EventStatus, Int](
    eventStatus => eventStatus.id,
    statusId => EventStatus.byId(statusId)
  )

  implicit val dateTimeMapper = MappedColumnType.base[DateTime, String](
    dateTime => dateTime.toString(ISODateTimeFormat.dateTime),
    isoFormat => DateTime.parse(isoFormat, ISODateTimeFormat.dateTime)
  )
}
