package com.coursework

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.coursework.api.UserEventApi
import com.coursework.exception.BaseExceptionHandler
import com.coursework.repository.EventRepository
import com.coursework.service.impl.{EventServiceImpl, EventStatusSchedulerServiceImpl}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

object MainHttpApplication {
  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher

  def main(args: Array[String]): Unit = {
    Await.result(MainApplication().start(), Duration.Inf)
    ()
  }
}

case class MainApplication()(implicit ac: ActorSystem, ec: ExecutionContext) extends LazyLogging {

  private val eventRepository: EventRepository = new EventRepository
  private val statusSchedulerService = new EventStatusSchedulerServiceImpl(eventRepository)
  private val eventService = new EventServiceImpl(eventRepository, statusSchedulerService)
  private val userEventApi: UserEventApi = new UserEventApi(eventService)

  private val routes = Route.seal(
    RouteConcatenation.concat(
      userEventApi.route
    )
  )(exceptionHandler = BaseExceptionHandler.exceptionHandler)

  def start(): Future[Http.ServerBinding] = {
    Http()
      .newServerAt("localhost", 8080)
      .bind(routes)
      .andThen { case b => logger.info(s"server started at: $b") }
  }
}
