package com.coursework.service.impl

import akka.actor.{ActorSystem, Cancellable}
import com.coursework.model._
import com.coursework.repository.{EventRepository, db}
import com.coursework.service.EventStatusSchedulerService
import com.github.nscala_time.time.Imports.{richReadableInstant, richReadableInterval}
import org.joda.time.DateTime

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{FiniteDuration, MILLISECONDS}

class EventStatusSchedulerServiceImpl(eventRepository: EventRepository)
                                     (implicit ac: ActorSystem, ec: ExecutionContext)
  extends EventStatusSchedulerService {

  private val scheduler = ac.scheduler
  private[impl] val eventStatusTasks = new TrieMap[Long, Cancellable]

  override def createTaskToStartEvent(event: Event): Unit =
    createTaskForEvent(
      event.plannedStartTime,
      db.run(eventRepository.updateStatusById(event.id.get, EventStatus.EventInProgress))
        .andThen(_ => createTaskToFinishEvent(event))
    )(event)

  override def createTaskToFinishEvent(event: Event): Unit =
    createTaskForEvent(
      event.plannedFinishTime,
      db.run(eventRepository.updateStatusById(event.id.get, EventStatus.EventFinished)))(event)

  override def stopTasksForEvent(event: Event): Unit =
    eventStatusTasks.remove(event.id.get) match {
      case Some(task) => task.cancel()
    }

  private def createTaskForEvent(startTime: DateTime, task: => Unit)(event: Event): Unit = {
    val scheduledTask = scheduler.scheduleOnce(FiniteDuration((DateTime.now to startTime).millis, MILLISECONDS))(task)
    eventStatusTasks.put(event.id.get, scheduledTask) match {
      case Some(task) => task.cancel()
    }
  }
}
