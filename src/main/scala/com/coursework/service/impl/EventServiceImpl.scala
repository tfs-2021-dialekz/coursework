package com.coursework.service.impl

import com.coursework.exception.EventNotFoundException
import com.coursework.model._
import com.coursework.repository.{EventRepository, db}
import com.coursework.service.{EventService, EventStatusSchedulerService}

import scala.collection.concurrent.TrieMap
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

class EventServiceImpl(
                        eventRepository: EventRepository,
                        schedulerService: EventStatusSchedulerService
                      )(implicit executor: ExecutionContext) extends EventService {

  private val syncEventIds = new TrieMap[Long, Long]

  override def getEventById(eventId: Long): Future[Event] =
    db.run(eventRepository.getById(eventId))
      .flatMap {
        case None => Future.failed(EventNotFoundException(eventId))
        case Some(event) => Future.successful(event)
      }

  override def createEvent(event: Event): Future[Event] =
    db.run(eventRepository.add(event))
      .andThen {
        case Success(event) => event.status match {
          case EventStatus.EventCreated => schedulerService.createTaskToStartEvent(event)
          case EventStatus.EventInProgress => schedulerService.createTaskToFinishEvent(event)
        }
      }

  override def updateEvent(eventId: Long, event: Event): Future[Event] =
    executeWithSyncByEventId[Event](eventId)(
      () => db.run(eventRepository.updateById(eventId, event))
        .flatMap(checkRowsAndReturnEvent(_, eventId))
        .andThen {
          case Success(event) => event.status match {
            case EventStatus.EventCreated => schedulerService.createTaskToStartEvent(event)
            case EventStatus.EventInProgress => schedulerService.createTaskToFinishEvent(event)
          }
        }
    )

  override def startEvent(eventId: Long): Future[Event] =
    executeWithSyncByEventId[Event](eventId)(
      () => {
        db.run(eventRepository.updateStatusById(eventId, EventStatus.EventInProgress))
          .flatMap(checkRowsAndReturnEvent(_, eventId))
          .andThen {
            case Success(event) => schedulerService.createTaskToFinishEvent(event)
          }
      }
    )

  override def finishEvent(eventId: Long): Future[Event] =
    executeWithSyncByEventId[Event](eventId)(
      () => db.run(eventRepository.updateStatusById(eventId, EventStatus.EventFinished))
        .flatMap(checkRowsAndReturnEvent(_, eventId))
    )

  override def rejectEvent(eventId: Long): Future[Event] =
    executeWithSyncByEventId[Event](eventId)(
      () => db.run(eventRepository.updateStatusById(eventId, EventStatus.EventRejected))
        .flatMap(checkRowsAndReturnEvent(_, eventId))
    )

  private def checkRowsAndReturnEvent(rows: Int, eventId: Long): Future[Event] = rows match {
    case 0 => Future.failed(EventNotFoundException(eventId))
    case _ => getEventById(eventId)
  }

  private def executeWithSyncByEventId[T](eventId: Long)(execute: () => Future[T]): Future[T] = {
    syncEventIds.getOrElseUpdate(eventId, eventId).synchronized {
      val res = execute.apply
      syncEventIds.remove(eventId)
      res
    }
  }
}
