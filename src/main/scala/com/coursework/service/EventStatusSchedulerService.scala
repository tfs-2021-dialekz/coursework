package com.coursework.service

import com.coursework.model.Event

trait EventStatusSchedulerService {
  def createTaskToStartEvent(event: Event): Unit
  def createTaskToFinishEvent(event: Event): Unit
  def stopTasksForEvent(event: Event): Unit
}
