package com.coursework

import com.coursework.model.{Event, EventStatus, EventType}
import org.joda.time.DateTime

trait BaseSampleData {

  val BASE_EVENT_ID: Long = 1

  lazy val sampleEvent = Event(
    id = Some(BASE_EVENT_ID),
    eventType = EventType.PlannedWork,
    status = EventStatus.EventCreated,
    plannedStartTime = DateTime.now.plus(1000),
    plannedFinishTime = DateTime.now.plus(10000)
  )

}
