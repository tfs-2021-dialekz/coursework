package com.coursework.repository

import com.coursework.BaseSampleData
import com.coursework.model._
import org.joda.time.DateTime
import org.scalatest.matchers.should.Matchers
import slick.jdbc.H2Profile.api._

class EventRepositoryTest extends EventRepositoryDatabaseSuite with Matchers with BaseSampleData {

  test("allEvents should contain all events") {
    for {
      events <- eventRepository.allEvents.result
    } yield events should contain theSameElementsAs SampleEvents
  }

  test("getById should return right event") {
    for {
      actual <- eventRepository.getById(BASE_EVENT_ID)
    } yield assert(actual.contains(SampleEvents.head))
  }

  test("add should return event with auto incremented id") {
    val sampleEvent = Event(None, EventType.PlannedWork, EventStatus.EventCreated, DateTime.now, DateTime.now.plus(10000))
    for {
      event <- eventRepository.add(sampleEvent)
      actual <- eventRepository.getById(event.id.get)
    } yield assert(actual.contains(sampleEvent.copy(id = Some(SampleEvents.size + 1))))
  }

  test("updateById should update event in database") {
    val expected = SampleEvents.head.copy(eventType = EventType.ServiceCrash, status = EventStatus.EventInProgress)
    for {
      rows <- eventRepository.updateById(SampleEvents.head.id.get, expected)
      actual <- eventRepository.getById(SampleEvents.head.id.get)
    } yield {
      assert(rows == 1)
      assert(actual.contains(expected))
    }
  }

  test("updateStatusById should update status in database") {
    val expected = SampleEvents.head.copy(status = EventStatus.EventRejected)
    for {
      rows <- eventRepository.updateStatusById(SampleEvents.head.id.get, EventStatus.EventRejected)
      actual <- eventRepository.getById(SampleEvents.head.id.get)
    } yield {
      assert(rows == 1)
      assert(actual.contains(expected))
    }
  }

}
