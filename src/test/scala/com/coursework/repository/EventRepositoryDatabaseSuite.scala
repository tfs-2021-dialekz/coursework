package com.coursework.repository

import com.coursework.BaseSampleData
import org.scalactic.source
import org.scalatest.compatible
import org.scalatest.funsuite.AsyncFunSuite
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import java.util.UUID

class EventRepositoryDatabaseSuite extends AsyncFunSuite with BaseSampleData {
  protected def test[R, S <: NoStream, E <: Effect](testName: String)
                                                   (testFun: DBIOAction[compatible.Assertion, S, E])
                                                   (implicit pos: source.Position): Unit = {
    super.test(testName) {

      val db = Database.forURL(
        s"jdbc:h2:mem:${UUID.randomUUID}",
        driver = "org.h2.Driver",
        keepAliveConnection = true
      )

      db.run(initSchema.andThen(eventRepository.allEvents ++= SampleEvents))
        .flatMap(_ => db.run(testFun))
        .andThen { case _ => db.close() }
    }
  }

  protected val eventRepository: EventRepository = new EventRepository

  private val initSchema = eventRepository.allEvents.schema.create

  protected val SampleEvents = Seq(
    sampleEvent
  )
}
