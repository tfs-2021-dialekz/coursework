package com.coursework.service.impl

import akka.actor.{ActorSystem, Cancellable}
import com.coursework.BaseSampleData
import com.coursework.repository.EventRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

import scala.concurrent.ExecutionContext

class EventStatusSchedulerServiceImplTest extends AnyFunSuite with MockFactory with BeforeAndAfter with BaseSampleData {

  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher

  private val mockEventRepository: EventRepository = mock[EventRepository]
  private val mockTask: Cancellable = mock[Cancellable]

  private val eventStatusSchedulerService: EventStatusSchedulerServiceImpl = new EventStatusSchedulerServiceImpl(mockEventRepository)

  before {
    eventStatusSchedulerService.eventStatusTasks.addOne(BASE_EVENT_ID, mockTask)
  }

  after {
    eventStatusSchedulerService.eventStatusTasks.clear()
  }

  test("stopTasksForEvent should cancel event if it exists") {
    (mockTask.cancel _).expects().once()
    eventStatusSchedulerService.stopTasksForEvent(sampleEvent)
    assert(!eventStatusSchedulerService.eventStatusTasks.contains(BASE_EVENT_ID))
  }

  test("createTaskToStartEvent") {
    (mockTask.cancel _).expects().once()
    eventStatusSchedulerService.createTaskToStartEvent(sampleEvent)
    assert(eventStatusSchedulerService.eventStatusTasks.contains(BASE_EVENT_ID))
  }

  test("createTaskToFinishEvent") {
    (mockTask.cancel _).expects().once()
    eventStatusSchedulerService.createTaskToFinishEvent(sampleEvent)
    assert(eventStatusSchedulerService.eventStatusTasks.contains(BASE_EVENT_ID))
  }
}
