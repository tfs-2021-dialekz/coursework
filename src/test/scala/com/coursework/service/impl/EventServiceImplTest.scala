package com.coursework.service.impl

import com.coursework.BaseSampleData
import com.coursework.exception.EventNotFoundException
import com.coursework.model.{Event, EventStatus}
import com.coursework.repository.EventRepository
import com.coursework.service.EventStatusSchedulerService
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import slick.dbio.SuccessAction

import scala.concurrent.ExecutionContext.Implicits.global

class EventServiceImplTest extends AnyFunSuite with MockFactory with Matchers with BaseSampleData {

  private val mockEventRepository: EventRepository = mock[EventRepository]
  private val mockEventStatusSchedulerService: EventStatusSchedulerService = mock[EventStatusSchedulerService]

  private val eventService: EventServiceImpl = new EventServiceImpl(mockEventRepository, mockEventStatusSchedulerService)

  test("getEventById should return event") {
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(Some(sampleEvent))
    for {
      actual <- eventService.getEventById(BASE_EVENT_ID)
    } yield assert(actual == sampleEvent)
  }

  test("getEventById should return EventNotFoundException") {
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(None)

    for {
      _ <- eventService.getEventById(BASE_EVENT_ID)
    } yield assertThrows(EventNotFoundException(BASE_EVENT_ID))
  }

  test("createEvent should return created event and call createTaskToStartEvent") {
    val eventWithoutId = sampleEvent.copy(id = None)
    mockEventRepository.add _ expects eventWithoutId returns SuccessAction(sampleEvent)

    for {
      actual <- eventService.createEvent(eventWithoutId)
    } yield (
      assert(actual == sampleEvent),
      (mockEventStatusSchedulerService.createTaskToStartEvent _).expects(sampleEvent).once()
    )
  }

  test("update should return updated event and call createTaskToFinishEvent") {
    val updatedEvent: Event = sampleEvent.copy(status = EventStatus.EventInProgress)
    mockEventRepository.updateById _ expects(BASE_EVENT_ID, updatedEvent) returns SuccessAction(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(Some(updatedEvent))
    mockEventStatusSchedulerService.createTaskToFinishEvent _ expects updatedEvent returns()

    for {
      actual <- eventService.updateEvent(BASE_EVENT_ID, updatedEvent)
    } yield (
      assert(actual == updatedEvent),
      (mockEventStatusSchedulerService.createTaskToFinishEvent _).expects(updatedEvent).once()
    )
  }

  test("startEvent should return updated event with status EventInProgress and call createTaskToFinishEvent") {
    val startedEvent: Event = sampleEvent.copy(status = EventStatus.EventInProgress)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventInProgress) returns SuccessAction(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(Some(startedEvent))

    for {
      actual <- eventService.startEvent(BASE_EVENT_ID)
    } yield (
      assert(actual == startedEvent),
      (mockEventRepository.updateStatusById _).expects(BASE_EVENT_ID, EventStatus.EventInProgress).once(),
      (mockEventStatusSchedulerService.createTaskToFinishEvent _).expects(startedEvent).once()
    )
  }

  test("finishEvent should return updated event with status EventFinished") {
    val finishedEvent: Event = sampleEvent.copy(status = EventStatus.EventFinished)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventFinished) returns SuccessAction(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(Some(finishedEvent))

    for {
      actual <- eventService.finishEvent(BASE_EVENT_ID)
    } yield assert(actual == finishedEvent)
  }

  test("rejectEvent should return updated event with status EventRejected") {
    val rejectedEvent: Event = sampleEvent.copy(status = EventStatus.EventRejected)
    mockEventRepository.updateStatusById _ expects(BASE_EVENT_ID, EventStatus.EventRejected) returns SuccessAction(1)
    mockEventRepository.getById _ expects BASE_EVENT_ID returns SuccessAction(Some(rejectedEvent))

    for {
      actual <- eventService.rejectEvent(BASE_EVENT_ID)
    } yield assert(actual == rejectedEvent)
  }
}
